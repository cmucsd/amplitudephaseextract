(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[     32826,        960]
NotebookOptionsPosition[     31837,        909]
NotebookOutlinePosition[     32215,        927]
CellTagsIndexPosition[     32172,        924]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Amplitude/phase extraction", "Title"],

Cell[TextData[{
 "An improvement of ",
 ButtonBox["US Patent 8169214 B2",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["https://www.google.com/patents/US8169214"], None},
  ButtonNote->"https://www.google.com/patents/US8169214"],
 "\nbased on an algorithm by Christoph Maier conceived in 2004"
}], "Subsubtitle"],

Cell[CellGroupData[{

Cell["Initialization", "Subsection"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"Plot", ",", 
    RowBox[{"Frame", "\[Rule]", "True"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"ListPlot", ",", 
    RowBox[{"Frame", "\[Rule]", "True"}]}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"SetOptions", "[", 
     RowBox[{"#", ",", 
      RowBox[{"BaseStyle", "\[Rule]", 
       RowBox[{"Directive", "[", 
        RowBox[{
         RowBox[{"FontFamily", "\[Rule]", "\"\<Helvetica\>\""}], ",", 
         RowBox[{"FontSize", "\[Rule]", "12"}]}], "]"}]}]}], "]"}], "&"}], "/@", 
   RowBox[{"{", 
    RowBox[{"Plot", ",", "ListPlot"}], "}"}]}], ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Amplitude phase extraction", "Section"],

Cell[CellGroupData[{

Cell["Function operating on array", "Subsection"],

Cell[TextData[{
 "Assuming equidistant samples of a sinusoid with ",
 StyleBox["normfreq",
  FontSlant->"Italic"],
 " the frequency of the waveform, normalized to the full length of the \
sampled sequence ",
 StyleBox["data",
  FontSlant->"Italic"],
 ". "
}], "Text"],

Cell[TextData[{
 "The sequences\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalSigma]c", ":=", 
    SubscriptBox[
     RowBox[{"{", 
      RowBox[{"cos", "(", 
       RowBox[{"2", "\[Pi]", " ", 
        FractionBox["normfreq", "samples"], "t"}], ")"}], "}"}], 
     RowBox[{"t", "\[Element]", 
      RowBox[{"(", 
       RowBox[{
        FractionBox[
         RowBox[{"1", "-", "samples"}], "2"], ",", 
        FractionBox[
         RowBox[{"3", "-", "samples"}], "2"], ",", "...", ",", 
        FractionBox[
         RowBox[{"samples", "-", "3"}], "2"], ",", 
        FractionBox[
         RowBox[{"samples", "-", "1"}], "2"]}], ")"}]}]]}], TraditionalForm]]],
 " \nand \n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalSigma]s", ":=", 
    SubscriptBox[
     RowBox[{"{", 
      RowBox[{"sin", "(", 
       RowBox[{"2", "\[Pi]", " ", 
        FractionBox["normfreq", "samples"], "t"}], ")"}], "}"}], 
     RowBox[{"t", "\[Element]", 
      RowBox[{"(", 
       RowBox[{
        FractionBox[
         RowBox[{"1", "-", "samples"}], "2"], ",", 
        FractionBox[
         RowBox[{"3", "-", "samples"}], "2"], ",", "...", ",", 
        FractionBox[
         RowBox[{"samples", "-", "3"}], "2"], ",", 
        FractionBox[
         RowBox[{"samples", "-", "1"}], "2"]}], ")"}]}]]}], TraditionalForm]]],
 "\nare computed and stored as constant vectors, where ",
 StyleBox["samples",
  FontSlant->"Italic"],
 " is the number of samples in the ",
 StyleBox["data",
  FontSlant->"Italic"],
 " vector."
}], "Text"],

Cell[TextData[{
 "The inverse of the covariance matrix of the two sequences is computed and \
stored as constant ",
 Cell[BoxData[
  FormBox[
   RowBox[{"2", "\[Cross]", "2"}], TraditionalForm]]],
 " matrix. \n",
 Cell[BoxData[
  FormBox[
   RowBox[{"M", "=", 
    SuperscriptBox[
     RowBox[{"(", GridBox[{
        {
         RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]c"}], 
         RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]c"}]},
        {
         RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]s"}], 
         RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]s"}]}
       }], ")"}], 
     RowBox[{"-", "1"}]]}], TraditionalForm]]]
}], "Text"],

Cell[TextData[{
 "A vector with the dot products of ",
 StyleBox["data",
  FontSlant->"Italic"],
 " with the cosine and sine sequences is computed:\[LineSeparator]",
 Cell[BoxData[
  FormBox[
   RowBox[{"d", "=", 
    RowBox[{"(", GridBox[{
       {
        RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "data"}]},
       {
        RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "data"}]}
      }], ")"}]}], TraditionalForm]]]
}], "Text"],

Cell[TextData[{
 "A vector with the in-phase and quadrature components of the sampled \
waveform is computed:\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"(", GridBox[{
       {"\[Xi]"},
       {"\[Upsilon]"}
      }], ")"}], "=", 
    RowBox[{
     RowBox[{"M", "\[CenterDot]", "d"}], "=", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", GridBox[{
          {
           RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]c"}], 
           RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]c"}]},
          {
           RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]s"}], 
           RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]s"}]}
         }], ")"}], 
       RowBox[{"-", "1"}]], "\[CenterDot]", 
      RowBox[{"(", GridBox[{
         {
          RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "data"}]},
         {
          RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "data"}]}
        }], ")"}]}]}]}], TraditionalForm]]]
}], "Text"],

Cell[TextData[{
 "The ",
 StyleBox["normfreq",
  FontSlant->"Italic"],
 " Fourier component of the sampled data has the amplitude\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"A", "=", 
    SqrtBox[
     RowBox[{
      SuperscriptBox["\[Xi]", "2"], "+", 
      SuperscriptBox["\[Upsilon]", "2"]}]]}], TraditionalForm]]],
 "\nand the phase (with respect to the center of the sampling interval)\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[Phi]", "=", 
    RowBox[{
     RowBox[{"arctan2", "(", 
      RowBox[{"\[Xi]", ",", "\[Upsilon]"}], ")"}], "=", 
     RowBox[{"\[PlusMinus]", 
      RowBox[{"arctan", "(", 
       FractionBox["\[Upsilon]", "\[Xi]"], ")"}]}]}]}], TraditionalForm]]],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"AmpPhase", "[", "normfreq_", "]"}], ":=", "\[IndentingNewLine]", 
  RowBox[{"Function", "[", 
   RowBox[{
    RowBox[{"{", "data", "}"}], ",", 
    RowBox[{"Block", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"samples", "=", 
         RowBox[{"Length", "[", "data", "]"}]}], ",", "\[CapitalSigma]c", ",",
         "\[CapitalSigma]s", ",", "mat", ",", "\[CapitalSigma]ci", ",", 
        "\[CapitalSigma]si", ",", "\[Xi]", ",", "\[Upsilon]"}], "}"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"\[CapitalSigma]c", "=", 
        RowBox[{"Table", "[", 
         RowBox[{
          RowBox[{"Cos", "[", 
           RowBox[{"2", "\[Pi]", " ", 
            FractionBox["normfreq", "samples"], "i"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"i", ",", 
            FractionBox[
             RowBox[{"1", "-", "samples"}], "2"], ",", 
            FractionBox[
             RowBox[{"samples", "-", "1"}], "2"]}], "}"}]}], "]"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{"\[CapitalSigma]s", "=", 
        RowBox[{"Table", "[", 
         RowBox[{
          RowBox[{"Sin", "[", 
           RowBox[{"2", "\[Pi]", " ", 
            FractionBox["normfreq", "samples"], "i"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"i", ",", 
            FractionBox[
             RowBox[{"1", "-", "samples"}], "2"], ",", 
            FractionBox[
             RowBox[{"samples", "-", "1"}], "2"]}], "}"}]}], "]"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{"mat", "=", 
        RowBox[{"Inverse", "[", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"\[CapitalSigma]c", ".", "\[CapitalSigma]c"}], ",", 
             RowBox[{"\[CapitalSigma]c", ".", "\[CapitalSigma]s"}]}], "}"}], 
           ",", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{"\[CapitalSigma]c", ".", "\[CapitalSigma]s"}], ",", 
             RowBox[{"\[CapitalSigma]s", ".", "\[CapitalSigma]s"}]}], "}"}]}],
           "}"}], "]"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{"\[CapitalSigma]ci", "=", 
        RowBox[{"\[CapitalSigma]c", ".", "data"}]}], ";", 
       RowBox[{"\[CapitalSigma]si", "=", 
        RowBox[{"\[CapitalSigma]s", ".", "data"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "}"}], "=", 
        RowBox[{"mat", ".", 
         RowBox[{"{", 
          RowBox[{"\[CapitalSigma]ci", ",", "\[CapitalSigma]si"}], "}"}]}]}], 
       ";", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         SqrtBox[
          RowBox[{
           RowBox[{"{", 
            RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "}"}], ".", 
           RowBox[{"{", 
            RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "}"}]}]], ",", 
         RowBox[{"ArcTan", "[", 
          RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "]"}], ",", "\[Xi]", ",", 
         "\[Upsilon]"}], "}"}]}]}], "]"}]}], "]"}]}]], "Input"],

Cell[CellGroupData[{

Cell["Example", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"data", "=", 
        RowBox[{"Table", "[", 
         RowBox[{
          RowBox[{"A", " ", 
           RowBox[{"Cos", "[", 
            RowBox[{
             RowBox[{"2", "\[Pi]", " ", 
              FractionBox["t", "8"]}], "-", 
             RowBox[{"\[Phi]", " ", "\[Degree]"}]}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"t", ",", 
            RowBox[{"-", 
             FractionBox["7", "2"]}], ",", 
            FractionBox["7", "2"]}], "}"}]}], "]"}]}], ",", "samples"}], 
      "}"}], ",", 
     RowBox[{
      RowBox[{"samples", "=", 
       RowBox[{"Length", "[", "data", "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"TableForm", "[", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"ListPlot", "[", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"Transpose", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"Range", "[", 
                 RowBox[{
                  FractionBox[
                   RowBox[{"1", "-", "samples"}], "2"], ",", 
                  FractionBox[
                   RowBox[{"samples", "-", "1"}], "2"]}], "]"}], ",", "#"}], 
               "}"}], "]"}], "&"}], "/@", 
            RowBox[{"{", 
             RowBox[{"data", ",", 
              RowBox[{"Table", "[", 
               RowBox[{
                RowBox[{"Cos", "[", 
                 RowBox[{"2", "\[Pi]", " ", 
                  FractionBox["1", "samples"], "i"}], "]"}], ",", 
                RowBox[{"{", 
                 RowBox[{"i", ",", 
                  FractionBox[
                   RowBox[{"1", "-", "samples"}], "2"], ",", 
                  FractionBox[
                   RowBox[{"samples", "-", "1"}], "2"]}], "}"}]}], "]"}], ",", 
              RowBox[{"Table", "[", 
               RowBox[{
                RowBox[{"Sin", "[", 
                 RowBox[{"2", "\[Pi]", " ", 
                  FractionBox["1", "samples"], "i"}], "]"}], ",", 
                RowBox[{"{", 
                 RowBox[{"i", ",", 
                  FractionBox[
                   RowBox[{"1", "-", "samples"}], "2"], ",", 
                  FractionBox[
                   RowBox[{"samples", "-", "1"}], "2"]}], "}"}]}], "]"}]}], 
             "}"}]}], ",", 
           RowBox[{"PlotMarkers", "\[Rule]", "Automatic"}], ",", 
           RowBox[{"PlotStyle", "\[Rule]", 
            RowBox[{"PointSize", "[", "Large", "]"}]}], ",", 
           RowBox[{"PlotRange", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
                RowBox[{"-", "4"}], ",", "4"}], "}"}], ",", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"-", "2"}], ",", "2"}], "}"}]}], "}"}]}], ",", 
           RowBox[{"ImageSize", "\[Rule]", "500"}]}], "]"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"StyleForm", "[", 
          RowBox[{
           RowBox[{"TableForm", "[", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
               "\"\<Amplitude\>\"", ",", "\"\<Phase\>\"", ",", "\"\<I\>\"", 
                ",", "\"\<Q\>\""}], "}"}], ",", 
              RowBox[{
               RowBox[{
                RowBox[{"AmpPhase", "[", "1", "]"}], "[", "data", "]"}], "/", 
               
               RowBox[{"{", 
                RowBox[{"1", ",", "\[Degree]", ",", "1", ",", "1"}], 
                "}"}]}]}], "}"}], "]"}], ",", 
           RowBox[{"FontFamily", "\[Rule]", "\"\<Helvetica\>\""}]}], "]"}]}], 
        "}"}], "]"}]}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"A", ",", "1.2"}], "}"}], ",", "0.001", ",", "2"}], "}"}], ",", 
   
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\[Phi]", ",", "45"}], "}"}], ",", 
     RowBox[{"-", "180"}], ",", "180"}], "}"}]}], "]"}]], "Input"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`A$$ = 1.2, $CellContext`\[Phi]$$ = 45, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`A$$], 1.2}, 0.001, 2}, {{
       Hold[$CellContext`\[Phi]$$], 45}, -180, 180}}, Typeset`size$$ = {
    500., {171., 176.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`A$858$$ = 
    0, $CellContext`\[Phi]$859$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`A$$ = 1.2, $CellContext`\[Phi]$$ = 45}, 
      "ControllerVariables" :> {
        Hold[$CellContext`A$$, $CellContext`A$858$$, 0], 
        Hold[$CellContext`\[Phi]$$, $CellContext`\[Phi]$859$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Block[{$CellContext`data = 
         Table[$CellContext`A$$ 
           Cos[2 Pi ($CellContext`t/8) - $CellContext`\[Phi]$$ 
             Degree], {$CellContext`t, -(7/2), 7/
            2}], $CellContext`samples}, $CellContext`samples = 
         Length[$CellContext`data]; TableForm[{
           ListPlot[
            Map[Transpose[{
               
               Range[(1 - $CellContext`samples)/2, ($CellContext`samples - 1)/
                2], #}]& , {$CellContext`data, 
              Table[
               Cos[
               2 Pi (1/$CellContext`samples) $CellContext`i], \
{$CellContext`i, (1 - $CellContext`samples)/2, ($CellContext`samples - 1)/2}], 
              Table[
               Sin[
               2 Pi (1/$CellContext`samples) $CellContext`i], \
{$CellContext`i, (1 - $CellContext`samples)/2, ($CellContext`samples - 1)/
                2}]}], PlotMarkers -> Automatic, PlotStyle -> 
            PointSize[Large], PlotRange -> {{-4, 4}, {-2, 2}}, ImageSize -> 
            500], 
           StyleForm[
            
            TableForm[{{
              "Amplitude", "Phase", "I", "Q"}, $CellContext`AmpPhase[
               1][$CellContext`data]/{1, Degree, 1, 1}}], FontFamily -> 
            "Helvetica"]}]], 
      "Specifications" :> {{{$CellContext`A$$, 1.2}, 0.001, 
         2}, {{$CellContext`\[Phi]$$, 45}, -180, 180}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{545., {250., 255.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Function operating on irregularly sampled array", "Subsection"],

Cell[TextData[{
 "Here, data is sampled at irregularly spaced points ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    RowBox[{"(", 
     RowBox[{
      SubscriptBox["t", "k"], ",", 
      SubscriptBox["d", "k"]}], ")"}], 
    RowBox[{"k", "\[Element]", 
     RowBox[{"(", 
      RowBox[{"1", ",", "\[Ellipsis]", ",", "samples"}], ")"}]}]], 
   TraditionalForm]]],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalSigma]c", ":=", 
    SubscriptBox[
     RowBox[{"{", 
      RowBox[{"cos", "(", 
       RowBox[{"2", "\[Pi]", " ", 
        FractionBox["normfreq", "samples"], 
        SubscriptBox["t", "k"]}], ")"}], "}"}], 
     RowBox[{"k", "\[Element]", 
      RowBox[{"(", 
       RowBox[{"1", ",", "\[Ellipsis]", ",", "samples"}], ")"}]}]]}], 
   TraditionalForm]]],
 " \nand \n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalSigma]s", ":=", 
    SubscriptBox[
     RowBox[{"{", 
      RowBox[{"sin", "(", 
       RowBox[{"2", "\[Pi]", " ", 
        FractionBox["normfreq", "samples"], 
        SubscriptBox["t", "k"]}], ")"}], "}"}], 
     RowBox[{"k", "\[Element]", 
      RowBox[{"(", 
       RowBox[{"1", ",", "\[Ellipsis]", ",", "samples"}], ")"}]}]]}], 
   TraditionalForm]]],
 "\nare computed and stored as constant vectors, where ",
 StyleBox["samples",
  FontSlant->"Italic"],
 " is the number of samples in the ",
 StyleBox["data",
  FontSlant->"Italic"],
 " vector."
}], "Text"],

Cell["The remaining computations are as before:", "Text"],

Cell[TextData[{
 "The inverse of the covariance matrix of the two sequences is computed and \
stored as constant ",
 Cell[BoxData[
  FormBox[
   RowBox[{"2", "\[Cross]", "2"}], TraditionalForm]]],
 " matrix. \n",
 Cell[BoxData[
  FormBox[
   RowBox[{"M", "=", 
    SuperscriptBox[
     RowBox[{"(", GridBox[{
        {
         RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]c"}], 
         RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]c"}]},
        {
         RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]s"}], 
         RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]s"}]}
       }], ")"}], 
     RowBox[{"-", "1"}]]}], TraditionalForm]]]
}], "Text"],

Cell[TextData[{
 "A vector with the dot products of ",
 StyleBox["data",
  FontSlant->"Italic"],
 " with the cosine and sine sequences is computed:\[LineSeparator]",
 Cell[BoxData[
  FormBox[
   RowBox[{"d", "=", 
    RowBox[{"(", GridBox[{
       {
        RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "data"}]},
       {
        RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "data"}]}
      }], ")"}]}], TraditionalForm]]]
}], "Text"],

Cell[TextData[{
 "A vector with the in-phase and quadrature components of the sampled \
waveform is computed:\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"(", GridBox[{
       {"\[Xi]"},
       {"\[Upsilon]"}
      }], ")"}], "=", 
    RowBox[{
     RowBox[{"M", "\[CenterDot]", "d"}], "=", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", GridBox[{
          {
           RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]c"}], 
           RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]c"}]},
          {
           RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "\[CapitalSigma]s"}], 
           RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "\[CapitalSigma]s"}]}
         }], ")"}], 
       RowBox[{"-", "1"}]], "\[CenterDot]", 
      RowBox[{"(", GridBox[{
         {
          RowBox[{"\[CapitalSigma]c", "\[CenterDot]", "data"}]},
         {
          RowBox[{"\[CapitalSigma]s", "\[CenterDot]", "data"}]}
        }], ")"}]}]}]}], TraditionalForm]]]
}], "Text"],

Cell[TextData[{
 "The ",
 StyleBox["normfreq",
  FontSlant->"Italic"],
 " Fourier component of the sampled data has the amplitude\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"A", "=", 
    SqrtBox[
     RowBox[{
      SuperscriptBox["\[Xi]", "2"], "+", 
      SuperscriptBox["\[Upsilon]", "2"]}]]}], TraditionalForm]]],
 "\nand the phase (with respect to the center of the sampling interval)\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[Phi]", "=", 
    RowBox[{
     RowBox[{"arctan2", "(", 
      RowBox[{"\[Xi]", ",", "\[Upsilon]"}], ")"}], "=", 
     RowBox[{"\[PlusMinus]", 
      RowBox[{"arctan", "(", 
       FractionBox["\[Upsilon]", "\[Xi]"], ")"}]}]}]}], TraditionalForm]]],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"AmpPhaseIrregular", "[", "normfreq_", "]"}], ":=", 
  "\[IndentingNewLine]", 
  RowBox[{"Function", "[", 
   RowBox[{
    RowBox[{"{", "data", "}"}], ",", 
    RowBox[{"Block", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"samples", "=", 
         RowBox[{"Length", "[", "data", "]"}]}], ",", 
        RowBox[{"times", "=", 
         RowBox[{
          RowBox[{"Transpose", "[", "data", "]"}], "\[LeftDoubleBracket]", 
          "1", "\[RightDoubleBracket]"}]}], ",", 
        RowBox[{"values", "=", 
         RowBox[{
          RowBox[{"Transpose", "[", "data", "]"}], "\[LeftDoubleBracket]", 
          "2", "\[RightDoubleBracket]"}]}], ",", "\[CapitalSigma]c", ",", 
        "\[CapitalSigma]s", ",", "mat", ",", "\[CapitalSigma]ci", ",", 
        "\[CapitalSigma]si", ",", "\[Xi]", ",", "\[Upsilon]"}], "}"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"\[CapitalSigma]c", "=", 
        RowBox[{
         RowBox[{
          RowBox[{"Cos", "[", 
           RowBox[{"2", "\[Pi]", " ", 
            FractionBox["normfreq", "samples"], "#"}], "]"}], "&"}], "/@", 
         "times"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{"\[CapitalSigma]s", "=", 
        RowBox[{
         RowBox[{
          RowBox[{"Sin", "[", 
           RowBox[{"2", "\[Pi]", " ", 
            FractionBox["normfreq", "samples"], "#"}], "]"}], "&"}], "/@", 
         "times"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{"mat", "=", 
        RowBox[{"Inverse", "[", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"\[CapitalSigma]c", ".", "\[CapitalSigma]c"}], ",", 
             RowBox[{"\[CapitalSigma]c", ".", "\[CapitalSigma]s"}]}], "}"}], 
           ",", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{"\[CapitalSigma]c", ".", "\[CapitalSigma]s"}], ",", 
             RowBox[{"\[CapitalSigma]s", ".", "\[CapitalSigma]s"}]}], "}"}]}],
           "}"}], "]"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{"\[CapitalSigma]ci", "=", 
        RowBox[{"\[CapitalSigma]c", ".", "values"}]}], ";", 
       RowBox[{"\[CapitalSigma]si", "=", 
        RowBox[{"\[CapitalSigma]s", ".", "values"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "}"}], "=", 
        RowBox[{"mat", ".", 
         RowBox[{"{", 
          RowBox[{"\[CapitalSigma]ci", ",", "\[CapitalSigma]si"}], "}"}]}]}], 
       ";", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         SqrtBox[
          RowBox[{
           RowBox[{"{", 
            RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "}"}], ".", 
           RowBox[{"{", 
            RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "}"}]}]], ",", 
         RowBox[{"ArcTan", "[", 
          RowBox[{"\[Xi]", ",", "\[Upsilon]"}], "]"}], ",", "\[Xi]", ",", 
         "\[Upsilon]"}], "}"}]}]}], "]"}]}], "]"}]}]], "Input"],

Cell[CellGroupData[{

Cell["Example", "Subsubsection"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"table", "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"RandomReal", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "4"}], ",", "4"}], "}"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "8"}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"data", "=", 
        RowBox[{
         RowBox[{
          RowBox[{"{", 
           RowBox[{"#", ",", 
            RowBox[{"A", " ", 
             RowBox[{"Cos", "[", 
              RowBox[{
               RowBox[{"2", "\[Pi]", " ", 
                FractionBox["1", "8"], "#"}], "-", 
               RowBox[{"\[Phi]", " ", "\[Degree]"}]}], "]"}]}]}], "}"}], 
          "&"}], "/@", "table"}]}], ",", "times", ",", "values", ",", 
       "samples"}], "}"}], ",", 
     RowBox[{
      RowBox[{"times", "=", 
       RowBox[{
        RowBox[{"Transpose", "[", "data", "]"}], "\[LeftDoubleBracket]", "1", 
        "\[RightDoubleBracket]"}]}], ";", 
      RowBox[{"values", "=", 
       RowBox[{
        RowBox[{"Transpose", "[", "data", "]"}], "\[LeftDoubleBracket]", "2", 
        "\[RightDoubleBracket]"}]}], ";", 
      RowBox[{"samples", "=", 
       RowBox[{"Length", "[", "data", "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"TableForm", "[", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"ListPlot", "[", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"Transpose", "[", 
              RowBox[{"{", 
               RowBox[{"times", ",", "#"}], "}"}], "]"}], "&"}], "/@", 
            RowBox[{"{", 
             RowBox[{"values", ",", 
              RowBox[{
               RowBox[{
                RowBox[{"Cos", "[", 
                 RowBox[{"2", "\[Pi]", " ", 
                  FractionBox["#", "8"]}], "]"}], "&"}], "/@", "times"}], ",", 
              RowBox[{
               RowBox[{
                RowBox[{"Sin", "[", 
                 RowBox[{"2", "\[Pi]", 
                  FractionBox["#", "8"]}], "]"}], "&"}], "/@", "times"}]}], 
             "}"}]}], ",", 
           RowBox[{"PlotMarkers", "\[Rule]", "Automatic"}], ",", 
           RowBox[{"PlotStyle", "\[Rule]", 
            RowBox[{"PointSize", "[", "Large", "]"}]}], ",", 
           RowBox[{"Filling", "\[Rule]", "Axis"}], ",", 
           RowBox[{"PlotRange", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
                RowBox[{"-", "4"}], ",", "4"}], "}"}], ",", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"-", "2"}], ",", "2"}], "}"}]}], "}"}]}], ",", 
           RowBox[{"ImageSize", "\[Rule]", "500"}]}], "]"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"StyleForm", "[", 
          RowBox[{
           RowBox[{"TableForm", "[", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
               "\"\<Amplitude\>\"", ",", "\"\<Phase\>\"", ",", "\"\<I\>\"", 
                ",", "\"\<Q\>\""}], "}"}], ",", 
              RowBox[{
               RowBox[{
                RowBox[{"AmpPhaseIrregular", "[", "1", "]"}], "[", "data", 
                "]"}], "/", 
               RowBox[{"{", 
                RowBox[{"1", ",", "\[Degree]", ",", "1", ",", "1"}], 
                "}"}]}]}], "}"}], "]"}], ",", 
           RowBox[{"FontFamily", "\[Rule]", "\"\<Helvetica\>\""}]}], "]"}]}], 
        "}"}], "]"}]}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"A", ",", "1.2"}], "}"}], ",", "0.001", ",", "2"}], "}"}], ",", 
   
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\[Phi]", ",", "45"}], "}"}], ",", 
     RowBox[{"-", "180"}], ",", "180"}], "}"}]}], "]"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "2.723372399508408`"}], ",", "1.540959451423479`", ",", 
   RowBox[{"-", "2.582334092105535`"}], ",", 
   RowBox[{"-", "1.0784860942526286`"}], ",", 
   RowBox[{"-", "3.120752612841427`"}], ",", "0.6346055313062013`", ",", 
   "1.6103592148874082`", ",", "0.6675610999574442`"}], "}"}]], "Output"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`A$$ = 1.2, $CellContext`\[Phi]$$ = 45, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`A$$], 1.2}, 0.001, 2}, {{
       Hold[$CellContext`\[Phi]$$], 45}, -180, 180}}, Typeset`size$$ = {
    500., {171., 176.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`A$922$$ = 
    0, $CellContext`\[Phi]$923$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`A$$ = 1.2, $CellContext`\[Phi]$$ = 45}, 
      "ControllerVariables" :> {
        Hold[$CellContext`A$$, $CellContext`A$922$$, 0], 
        Hold[$CellContext`\[Phi]$$, $CellContext`\[Phi]$923$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Block[{$CellContext`data = 
         Map[{#, $CellContext`A$$ 
            Cos[2 Pi (1/8) # - $CellContext`\[Phi]$$ 
              Degree]}& , $CellContext`table], $CellContext`times, \
$CellContext`values, $CellContext`samples}, $CellContext`times = Part[
           Transpose[$CellContext`data], 1]; $CellContext`values = Part[
           Transpose[$CellContext`data], 2]; $CellContext`samples = 
         Length[$CellContext`data]; TableForm[{
           ListPlot[
            Map[Transpose[{$CellContext`times, #}]& , {$CellContext`values, 
              Map[Cos[2 Pi (#/8)]& , $CellContext`times], 
              Map[Sin[2 Pi (#/8)]& , $CellContext`times]}], PlotMarkers -> 
            Automatic, PlotStyle -> PointSize[Large], Filling -> Axis, 
            PlotRange -> {{-4, 4}, {-2, 2}}, ImageSize -> 500], 
           StyleForm[
            TableForm[{{
              "Amplitude", "Phase", "I", "Q"}, $CellContext`AmpPhaseIrregular[
               1][$CellContext`data]/{1, Degree, 1, 1}}], FontFamily -> 
            "Helvetica"]}]], 
      "Specifications" :> {{{$CellContext`A$$, 1.2}, 0.001, 
         2}, {{$CellContext`\[Phi]$$, 45}, -180, 180}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{545., {250., 255.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1581, 910},
ShowCellBracket->False,
Deployed->True,
CellContext->Notebook,
TrackCellChangeTimes->False,
FrontEndVersion->"8.0 for Linux x86 (32-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1257, 32, 43, 0, 88, "Title"],
Cell[1303, 34, 317, 8, 53, "Subsubtitle"],
Cell[CellGroupData[{
Cell[1645, 46, 36, 0, 39, "Subsection"],
Cell[1684, 48, 713, 21, 69, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2434, 74, 45, 0, 74, "Section"],
Cell[CellGroupData[{
Cell[2504, 78, 49, 0, 39, "Subsection"],
Cell[2556, 80, 267, 9, 30, "Text"],
Cell[2826, 91, 1516, 48, 141, "Text"],
Cell[4345, 141, 709, 20, 61, "Text"],
Cell[5057, 163, 433, 14, 63, "Text"],
Cell[5493, 179, 1004, 29, 63, "Text"],
Cell[6500, 210, 698, 23, 106, "Text"],
Cell[7201, 235, 3049, 78, 249, "Input"],
Cell[CellGroupData[{
Cell[10275, 317, 32, 0, 28, "Subsubsection"],
Cell[CellGroupData[{
Cell[10332, 321, 4016, 108, 196, "Input"],
Cell[14351, 431, 2955, 63, 522, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[17367, 501, 69, 0, 39, "Subsection"],
Cell[17439, 503, 1400, 48, 134, "Text"],
Cell[18842, 553, 57, 0, 30, "Text"],
Cell[18902, 555, 709, 20, 61, "Text"],
Cell[19614, 577, 433, 14, 63, "Text"],
Cell[20050, 593, 1004, 29, 63, "Text"],
Cell[21057, 624, 698, 23, 106, "Text"],
Cell[21758, 649, 2972, 75, 249, "Input"],
Cell[CellGroupData[{
Cell[24755, 728, 32, 0, 28, "Subsubsection"],
Cell[CellGroupData[{
Cell[24812, 732, 3837, 105, 213, "Input"],
Cell[28652, 839, 353, 7, 30, "Output"],
Cell[29008, 848, 2765, 54, 522, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature cvDRmdWI05zxEDwjX7B#XSTI *)
